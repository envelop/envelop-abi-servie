FROM nginx

COPY ./abis/ /usr/share/nginx/html/app/
COPY ./nginx/ /etc/nginx/templates/

#COPY docker-compose-prod.yaml /build

#VOLUME ["/build"]
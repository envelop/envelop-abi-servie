## Docker Dev Environment with local NGINX
```bash
# set app URL 
export PUBLIC_URL=/abis
```

```shell
docker-compose -f docker-compose-local.yaml up

$ # Traefik
$ docker-compose -f docker-compose-local2.yaml up
```
http://localhost:8080/abis/1/0x15a623559e29c0fc39b9561c3fd304b677e66087.json


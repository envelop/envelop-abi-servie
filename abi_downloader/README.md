# ABI Downloader

1. Put list of contract addresses to ./abi_downloader/queue.json according to chain id
2. Run from project root `python3 ./abi_downloader`
3. In case of NOTOK responses increase `delayBetweenCalls` in ./abi_downloader/config.json
4. Abi files will appear in abi folder according to chain id
5. Successfully downloaded abis will be cleared from queue file automatically
6. Push changes to remote repository

### Example of queue file

```json
{
    "1": [
        "0x53c55bB901812551aa36cbf022B5df35B24C9f59",
        "0x27772e884e9d238F5478c07D44C515bcae585b1d"
    ],
    "11155111": [
        "0xf544BB8427DC23EC69034C203b2B09860812DDfa",
        "0x50BddB7911CE4248822a60968A32CDF1D683e7AD"
    ]
}
```
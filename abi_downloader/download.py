
import os
import requests
from urllib.parse import urljoin
import json
import time

CONFIG_FILE_NAME = './abi_downloader/config.json'
LOCAL_CONFIG_FILE_NAME = './abi_downloader/local.config.json'
QUEUE_FILE_NAME = './abi_downloader/queue.json'
ABI_FOLDER_NAME = './abis/'

class TERMINAL_COLORS:
	OK      = '\033[92m'
	WARNING = '\033[93m'
	FAIL    = '\033[91m'
	ENDC    = '\033[0m'

def load_config():
	output = {}
	try:
		with open(CONFIG_FILE_NAME) as f:
			output.update(json.load(f))
	except Exception:
		print('Cannot load config file')

	try:
		with open(LOCAL_CONFIG_FILE_NAME) as f:
			output.update(json.load(f))
	except Exception:
		print('Cannot load config file')

	return output
config = load_config()

def load_queue():
	with open(QUEUE_FILE_NAME) as f:
		_queue = json.load(f)
		for chain_id in _queue.keys():
			_queue[chain_id] = [ x.lower() for x in _queue[chain_id] ]

		return _queue
queue = load_queue()

def update_json_file(queue):
	with open(QUEUE_FILE_NAME, 'w', encoding='utf-8') as f:
		json.dump(queue, f, ensure_ascii=False, indent=4)

def get_contract_url(chain_id: str, contract_address: str):
	if config['explorer'][chain_id] == 'Unsupported chain':
		raise Exception('Unsupported chain')

	apiKeys = ''
	try:
		apiKeys = '&'.join([f'{list(x.items())[0][0]}={list(x.items())[0][1]}' for x in config['queryParams'][chain_id]])
	except Exception:
		pass
	return urljoin(config['explorer'][chain_id], f'/api?module=contract&action=getabi&address={contract_address}&format=raw&{apiKeys}')

def fetch_abi(url: str):
	response = requests.get(url)
	if not response.ok:
		raise Exception(f'Cannot fetch abi: {response.status_code}')
	data = response.json()  # Convert the response to JSON

	error_msg = ''
	if 'message' in data:
		error_msg = error_msg + data['message'] + '; '
	if 'result' in data:
		error_msg = error_msg + data['result'] + '; '
	if error_msg != '':
		raise Exception(f'Cannot fetch abi: {error_msg}')

	return data

def save_abi(chain_id: str, contract_address: str, data):
	filename = urljoin(ABI_FOLDER_NAME, f'{chain_id}/{contract_address}.json')
	os.makedirs(os.path.dirname(filename), exist_ok=True)
	with open(filename, "w") as f:
		json.dump(data, f)

def main():
	delayBetweenCalls = 1000 if 'delayBetweenCalls' not in config else config['delayBetweenCalls']
	updated_queue = queue

	for chain_id in queue.keys():
		print('Parsing chain: ', chain_id)

		succesfully_fetched = []
		for contract_address in queue[chain_id]:

			try:
				print('Fetching contract: ', contract_address)
				url = get_contract_url(chain_id, contract_address)
				# print('url', url)
				abi = fetch_abi(url)
				save_abi(chain_id, contract_address, abi)
				succesfully_fetched.append(contract_address)
				time.sleep(delayBetweenCalls)
			except Exception as e:
				print(f'''{TERMINAL_COLORS.FAIL}Cannot fetch abi: {e}{TERMINAL_COLORS.ENDC}''')

		# Check if all contracts of chain has been downloaded
		errored_addresses = []
		for item in updated_queue[chain_id]:
			if item not in succesfully_fetched:
				errored_addresses.append(item)
		updated_queue[chain_id] = errored_addresses


	print()
	print('-'*30)
	print()

	for chain_id in updated_queue:
		if len(updated_queue[chain_id]) == 0:
			print(f'''{TERMINAL_COLORS.OK}Chain {chain_id} has been completely downloaded{TERMINAL_COLORS.ENDC}''')
		else:
			print(f'''{TERMINAL_COLORS.FAIL}Some errors occured while downloading chain {chain_id}{TERMINAL_COLORS.ENDC}''')

	print()
	update_json_file(updated_queue)
	print('Queue file has been updated')
	# print('updated_queue', json.dumps(updated_queue, indent=4))
	# print('----')





